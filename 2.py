from datetime import datetime
def timeit(func):
    def whrapper():
        start = datetime.now()
        result = func()
        print(datetime.now() - start)
        return result
    return whrapper
@timeit
def one():
    I = []
    for i in range(10**6):
        if i % 2 == 0:
            I.append(i)
  
    return i
i1 = one()
