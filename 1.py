from functools import wraps
count = {}
def counter(list):
    count[list.__name__] = 0
    @wraps(list)
    def wrapper(*args, **kwargs):
        count[list.__name__] += 1
        return list(*args, **kwargs)
    return wrapper
@counter
def data():
    print('Функция first')
@counter
def main():
    print('Функиця second')

@counter
def run():
    print('Функиця second')

data()
data()
run()
main()

print('Количество вызывов функций: ',count)
